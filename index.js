const express = require('express');
const app = express();
const port = 3000

app.set("view engine", "ejs");

app.get('/', (req, res) => {
  const name = "zaky";
  res.render('index', { name })
})

app.get('/berita', (req, res) => res.send([
  {
    id: 2011,
    titel: "Mobil ambulance ketabrak",
    news: "Mobil ambulance ketabrak 2 jenazah tewas"
  },
  {
    id: 2011,
    titel: "Mobil ambulance ketabrak",
    news: "Mobil ambulance ketabrak 2 jenazah tewas"
  }
]));

app.get('/bola', (req, res) => res.send({
  id: 123,
  title: "Juventur membeli ronaldo",
  "news": "ronaldo di beli juventus 200ribu rupiah",
}));

app.get('/artis', (req, res) => res.send({
  id: 123,
  title: "sule pelawak terkaya",
  news: "kekayaan sule bla bla bla"
}))

app.listen(port, () => console.log(`Aplikasi sudah jalan di port ${port}`));