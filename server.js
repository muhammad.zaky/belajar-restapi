const express = require('express');
const app = express();
const berita = require('./db/berita.json')
app.use(express.json());

app.get('/api/v1/berita', (req, res) => {
  res.status(200).json(berita)
});

app.get('/api/v1/berita/:id', (req, res) => {
  const idBerita = parseInt(req.params.id);
  const detailBerita = berita.filter((item) => item.id === idBerita);
  res.status(200).json(detailBerita);
});

app.post('/api/v1/berita', (req, res) => {
  const { title, body } = req.body;
  const idBerita = berita.length + 1;
  const result = {
    id: idBerita,
    title,
    body
  };

  res.status(200).json(result);
})

app.put('/api/v1/berita/:id', (req, res) => {
  const idBerita = parseInt(req.params.id);
  const { title } = req.body;
  const detailBerita = berita.filter((item) => item.id === idBerita);
  const result = {
    id: idBerita,
    title,
    body: detailBerita[0].body
  }

  res.status(200).json(result);
});

app.delete('/api/v1/berita/:id', (req, res) => {
  const idBerita = parseInt(req.params.id);
  const detailBerita = berita.filter((item) => item.id !== idBerita);
  res.status(200).json(detailBerita);
})

app.listen(3000, () => {
  console.log("apps berjalan di port 3000")
})